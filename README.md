# HISTORY #

### v1.0.5 ###

* Adds Installments support.

### v1.0.4 ###

* Removes 3d Secure Url fix.

### v1.0.3 ###

* Make cofiguration visible in store level.

### v1.0.2 ###

* Adds 3D Secure URL.

### v1.0.1 ###

* Adds default payment & ticket url.
* Checks if table exists before creating it.

### v1.0.0 ###

* Working release.


## What is this repository for? ##

* Module that implements Winbank payment method.


## Installation Instructions ##

1. Enable "Allow Symlinks" (System > Configuration > Advanced > Developer)
2. Install modman (if not exists) ``` bash < <(wget -q --no-check-certificate -O - https://raw.github.com/colinmollenhour/modman/master/modman-installer) ```
3. Switch to Magento root folder
4. ``` modman init ```
5. ``` modman clone https://convergeict@bitbucket.org/converge_ict/winbank.git ```
6. Clear cache