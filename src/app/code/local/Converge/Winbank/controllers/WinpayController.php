<?php

class Converge_Winbank_WinpayController extends Mage_Core_Controller_Front_Action {

  protected function _expireAjax()
  {
    if (!Mage::getSingleton('checkout/session')->getQuote()->hasItems()) 
    {
      $this->getResponse()->setHeader('HTTP/1.1','403 Session Expired');
      exit;
    }
  }
	
	public function getWinpay() {
    return Mage::getSingleton('winbank/winpay');
  }

  public function redirectAction() 
  {
    $session = Mage::getSingleton('checkout/session');
    $session->setWinpayQuoteId($session->getQuoteId());
    $this->getResponse()->setBody($this->getLayout()->createBlock('winbank/Winpay_Redirect')->toHtml());
    //unset the quoteId
    $session->unsQuoteId();
  }

	
	
  

  //sucess
  public function  successAction() 
  {
    $session = Mage::getSingleton('checkout/session');
    $session->setQuoteId($session->getWinpayQuoteId(true));
    $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();

    $debug = Mage::getModel('paygate/authorizenet_debug');  


    $getData = $this->getRequest()->getParams();

    Mage::log("get data ".print_r($getData,1),null,'winbnk.log');

    $SupportReferenceID = urldecode($getData["SupportReferenceID"]);      //different value for each request
    $ResultCode         = urldecode($getData["ResultCode"]);              // 0: ok, not 0: issue 
    $StatusFlag         = urldecode($getData["StatusFlag"]);              // Success | Failure             
    $ResponseCode       = urldecode($getData["ResponseCode"]);            // 00, 08, 10, 11, 16

    $ResponseDescription = "";                                            //error description
    if(isset($getData["ResponseDescription"]))
      $ResponseDescription = urldecode($getData["ResponseDescription"]);
    

    $MerchantReference  = urldecode($getData["MerchantReference"]);       
    
    $TransactionId      = urldecode($getData["TransactionId"]);           //unique transaction id
    $PackageNo          = urldecode($getData["PackageNo"]);       //transaction package id
    $ApprovalCode       = urldecode($getData["ApprovalCode"]);            //approval transaction code 

    $AuthStatus         = urldecode($getData["AuthStatus"]);              //3D secure 01: Visa|mastercard ok, 02: not (Visa|mastercard) ok, 03: fail
    $HashKey            = urldecode($getData["HashKey"]); 
    
    $Parameters         = urldecode($getData["Parameters"]); 
    


    //save data in $debug table: paygate_authorizenet_debug
    $debug->setResponseBody(serialize($getData));
    $debug->setResultSerialized(serialize(array("SupportReferenceID"=>$SupportReferenceID,"MerchantReference"=>$MerchantReference,"ResultCode"=>$ResultCode,"ResponseCode"=>$ResponseCode,"ResponseDescription"=>$ResponseDescription)));
    $debug->setResultDump($orderId);
    $debug->setRequestDump($StatusFlag);
    $debug->setRequestBody($MerchantReference);
    $debug->save();
    
    if(!$this->isHashKeyCorrect($HashKey, $ApprovalCode, $Parameters, $ResponseCode, $SupportReferenceID, $AuthStatus, $PackageNo, $StatusFlag))
    {
      
      //error wrong hashkey match -> cancel the order -> go to failure url
      $this->cancelOrder();
      Mage::log("ERR:WRONG HASH MATCH, order no $orderId was canceled, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");

      $this->_redirect('checkout/onepage/failure');  
      //send email with 
      //SupportReferenceID, MerchantReference, ResultCode, ResponseCode,ResponseDescription
      //set status for order pending
    }
    else
    {
       $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
       $order->getSendConfirmation(null);
       $order->sendNewOrderEmail();
      //everything is fine
      if($ResultCode==0 && $StatusFlag=="Success" && $ResponseCode != 11)
      {



        //invoice order set status in pending
        if($this->createInvoice($TransactionId))
          $this->_redirect('checkout/onepage/success');
        else
        {
          $this->cancelOrder();  
          Mage::log("ERR:ORDER CANNOT BE INVOICED, order no $orderId was canceled, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");
          $this->_redirect('checkout/onepage/failure');
        }  
        //send email with 
        //SupportReferenceID,MerchantReference, StatusFlag, ResponseCode, PackageNo, ApprovalCode, AuthStatus
        //set status for order cancel winbank card
      }
      else if($ResultCode==0 && $StatusFlag=="Success" && $ResponseCode == 11) //double charge
      {
        Mage::log("WARN:ORDER DOUBLE CHARGE, order no $orderId was invoiced, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");

        $paymentModel = Mage::getModel("winbank/winpay");


        $supportEmails      = $paymentModel->getSupportEmails();
        
        Mage::log("support emails: $supportEmails ",null,"winbnk.log");

        @mail($supportEmails , "Double Charge found for the order #$orderId" , "This order tried to get charged more than once, in bank didnt charge, magento cancel the order" );

        //invoice order set status in pending
        $this->cancelOrder();  
        Mage::log("ERR:ORDER CANNOT BE DOUBLE CHARGED, order no $orderId was canceled, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");
          $this->_redirect('checkout/onepage/success');
          
        //send email with 
        //SupportReferenceID,MerchantReference, StatusFlag, ResponseCode, PackageNo, ApprovalCode, AuthStatus
        //set status for order cancel winbank card
        
      }
      else
      {
        //error (usually you cannot reach here since the bank will automatic forward to the failure url) -> cancel the order -> go to failure url
        $this->cancelOrder();
        Mage::log("ERR:UNEXPECTED, order no $orderId was canceled, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");
        $this->_redirect('checkout/onepage/failure');
        //send email with 
        //SupportReferenceID, MerchantReference, ResultCode, ResponseCode,ResponseDescription

        //set status for order cancel winbank card
      }
    }

    
  }
  
  //fail
  public function  failAction() 
  {
    $session = Mage::getSingleton('checkout/session');
    $session->setQuoteId($session->getWinpayQuoteId(true));
    $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();


    $debug = Mage::getModel('paygate/authorizenet_debug');  


    $getData = $this->getRequest()->getParams();

    Mage::log("get data ".print_r($getData,1),null,'winbnk.log');

    $SupportReferenceID = urldecode($getData["SupportReferenceID"]);      //different value for each request
    $ResultCode         = urldecode($getData["ResultCode"]);              // 0: ok, not 0: issue 
    $StatusFlag         = urldecode($getData["StatusFlag"]);              // Success | Failure             
    $ResponseCode       = urldecode($getData["ResponseCode"]);            // 00, 08, 10, 11, 16

    $ResponseDescription = "";                                            //error description
    if(isset($getData["ResponseDescription"]))
      $ResponseDescription = urldecode($getData["ResponseDescription"]);
    

    $MerchantReference  = urldecode($getData["MerchantReference"]);       
    
    $TransactionId      = urldecode($getData["TransactionId"]);           //unique transaction id
    $PackageNo          = urldecode($getData["PackageNo"]);       //transaction package id
    $ApprovalCode       = urldecode($getData["ApprovalCode"]);            //approval transaction code 

    $AuthStatus         = urldecode($getData["AuthStatus"]);              //3D secure 01: Visa|mastercard ok, 02: not (Visa|mastercard) ok, 03: fail
    $HashKey            = urldecode($getData["HashKey"]); 
    
    $Parameters         = urldecode($getData["Parameters"]); 
    


    //save data in $debug table: paygate_authorizenet_debug
    $debug->setResponseBody(serialize($getData));
    $debug->setResultSerialized(serialize(array("SupportReferenceID"=>$SupportReferenceID,"MerchantReference"=>$MerchantReference,"ResultCode"=>$ResultCode,"ResponseCode"=>$ResponseCode,"ResponseDescription"=>$ResponseDescription)));
    $debug->setResultDump($orderId);
    $debug->setRequestDump($StatusFlag);
    $debug->setRequestBody($MerchantReference);
    $debug->save();

    //cancel the order -> go to failure url
    $this->cancelOrder();
     Mage::log("ERR:TECH FAILURE, order no $orderId was canceled, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");


    $this->_redirect('checkout/onepage/failure');
  }


  //cancel
  public function cancelAction() 
  {
    $session = Mage::getSingleton('checkout/session');
    $session->setQuoteId($session->getWinpayQuoteId(true));
    $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();


    $debug = Mage::getModel('paygate/authorizenet_debug');  


    $getData = $this->getRequest()->getParams();

    Mage::log("get data ".print_r($getData,1),null,'winbnk.log');

    $SupportReferenceID = urldecode($getData["SupportReferenceID"]);      //different value for each request
    $ResultCode         = urldecode($getData["ResultCode"]);              // 0: ok, not 0: issue 
    $StatusFlag         = urldecode($getData["StatusFlag"]);              // Success | Failure             
    $ResponseCode       = urldecode($getData["ResponseCode"]);            // 00, 08, 10, 11, 16

    $ResponseDescription = "";                                            //error description
    if(isset($getData["ResponseDescription"]))
      $ResponseDescription = urldecode($getData["ResponseDescription"]);
    

    $MerchantReference  = urldecode($getData["MerchantReference"]);       
    
    $TransactionId      = urldecode($getData["TransactionId"]);           //unique transaction id
    $PackageNo          = urldecode($getData["PackageNo"]);       //transaction package id
    $ApprovalCode       = urldecode($getData["ApprovalCode"]);            //approval transaction code 

    $AuthStatus         = urldecode($getData["AuthStatus"]);              //3D secure 01: Visa|mastercard ok, 02: not (Visa|mastercard) ok, 03: fail
    $HashKey            = urldecode($getData["HashKey"]); 
    
    $Parameters         = urldecode($getData["Parameters"]); 
    


    //save data in $debug table: paygate_authorizenet_debug
    $debug->setResponseBody(serialize($getData));
    $debug->setResultSerialized(serialize(array("SupportReferenceID"=>$SupportReferenceID,"MerchantReference"=>$MerchantReference,"ResultCode"=>$ResultCode,"ResponseCode"=>$ResponseCode,"ResponseDescription"=>$ResponseDescription)));
    $debug->setResultDump($orderId);
    $debug->setRequestDump($StatusFlag);
    $debug->setRequestBody($MerchantReference);
    $debug->save(); 

    //cancel the order -> go to checkout cart
     Mage::log("ERR:TYPICAL CANCELATION, order no $orderId was canceled, SupportReferenceID: $SupportReferenceID, MerchantReference: $MerchantReference, ResultCode: $ResultCode, ResponseCode: $ResponseCode, ResponseDescription: $ResponseDescription",null,"winbnk.log");
    $this->cancelOrder();

    $this->_redirect('checkout/cart');
  }


  //check if hash key match to the data
  private function isHashKeyCorrect($HashKey, $ApprovalCode, $Parameters, $ResponseCode, $SupportReferenceID, $AuthStatus, $PackageNo, $StatusFlag)
  {
    $session      = Mage::getSingleton('checkout/session');
    $paymentModel = Mage::getModel("winbank/winpay");


    $transactionTicket      = $session->getWinTicket();
    $posId                  = $paymentModel->getWinPosid();
    $acquirerId             = $paymentModel->getWinAcquirerid();
    $merchantReference      = $session->getMerchantReference();

    $dataArray = array(
        $transactionTicket,
        $posId,
        $acquirerId,
        $merchantReference,
        $ApprovalCode,
        $Parameters,
        $ResponseCode,
        $SupportReferenceID,
        $AuthStatus,
        $PackageNo,
        $StatusFlag
    );

    $hashData = implode(';', $dataArray);

    $myHashKey = strToUpper(hash_hmac('sha256', $hashData, $transactionTicket, false));
    Mage::log(" MyHashKey : $myHashKey", null,"winbnk.log");
    Mage::log(" HashKey : $HashKey", null,"winbnk.log");


    if($myHashKey === $HashKey)
      return true;
    else
      return false;

  }


  private function cancelOrder()
  {
    $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
    Mage::log("@@ in setOrderInvoice() orderId = $orderId", null, "winbnk.log");
    
    $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);


    $orderState = $order->getState();
    if ($orderState === Mage_Sales_Model_Order::STATE_PENDING_PAYMENT) 
    { // Check for state pending payment.

      if($order->canCancel())
        $order->cancel()->save();
      else
        Mage::log("order no : $orderId cannot be canceled", null, "winbnk.log");
    }
    
  }

  private function createInvoice($TransactionId = null)
  {
    $orderId = Mage::getSingleton('checkout/session')->getLastRealOrderId();
    Mage::log("@@ in setOrderInvoice() orderId = $orderId", null, "winbnk.log");
    
    $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);


    $orderState = $order->getState();
    if ($orderState === Mage_Sales_Model_Order::STATE_PENDING_PAYMENT) 
    { // Check for state pending payment.

      try 
      {
        if(!$order->canInvoice())
        {
          Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
          return false;
        }
        $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
        
        if (!$invoice->getTotalQty()) 
        {
          Mage::log("@@ in setOrderInvoice() empty order", null, "winbnk.log");
          Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
        }


        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $invoice->register();
        $transactionSave = Mage::getModel('core/resource_transaction')
        ->addObject($invoice)
        ->addObject($invoice->getOrder());
         
        $transactionSave->save();
        


        $order = Mage::getModel('sales/order')->load($order->getId());

        
        $orderComment = 'Winbank Confirmed Transaction<br />';
        $order->setState('processing', true, $orderComment);
                
        $order->save();  //to retain confirmation 
                
        $payment = $order->getPayment();
                

        $amount = $order->getGrandTotal();
        
        //Mage::log("!!!$amount",null,"winbnk.log");  
                                
        $payment->setTransactionId($TransactionId)
            ->setIsTransactionClosed(1)
            ->registerCaptureNotification($amount,true)
        ;
                                
        $order->save();

        // notify customer
        if ($invoice = $payment->getCreatedInvoice() && !$order->getEmailSent()) {
                $comment = $order->sendNewOrderEmail()->addStatusHistoryComment(
                    Mage::helper('sales')->__('Notified customer about invoice')
                )                   
                ->setIsCustomerNotified(true)
                ->save();
        }           
        $order->save(); 

        if (!is_null($TransactionId)) 
        {
          $order->addStatusHistoryComment('Winbank\'s Transaction ID: ' . $TransactionId);

          if(Mage::getSingleton('core/session')->getWinInstallments()){
            $installments = Mage::getSingleton('core/session')->getWinInstallments();
            $order->addStatusHistoryComment('Number of Installments: ' . $installments);
            Mage::getSingleton('core/session')->unsWinInstallments();
          }

          $order->save();
        }


        
        return true;
      }
      catch (Mage_Core_Exception $e) 
      {
        Mage::log("@@ in setOrderInvoice() unexpected error", null, "winbnk.log");
        Mage::throwException(Mage::helper('core')->__('Cannot create an invoice.'));
        return false;
      }    
    }

    if ($order->hasInvoices()) {
        Mage::log("Order already invoiced $orderId", null, "winbnk.log");
        return true;
    }

    Mage::log("@@ in setOrderInvoice() different state $orderState", null, "winbnk.log");
    return false;
  }

}
