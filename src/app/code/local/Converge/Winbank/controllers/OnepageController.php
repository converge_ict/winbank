<?php
require_once Mage::getModuleDir('controllers', 'Mage_Checkout').DS.'OnepageController.php';

class Converge_Winbank_OnepageController extends Mage_Checkout_OnepageController
{
	    
	public function saveOrderAction(){
		$installments = $this->getRequest()->getPost('win_installments');

		if($installments)
			Mage::getSingleton('core/session')->setWinInstallments($installments);

		parent::saveOrderAction();
	}
    
    
}