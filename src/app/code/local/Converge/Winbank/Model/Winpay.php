<?php

class Converge_Winbank_Model_Winpay extends Mage_Payment_Model_Method_Abstract {
  

  protected $_code                = 'winpay';
  protected $_isInitializeNeeded  = true;
  protected $_canCapture          = true;

  protected $_formBlockType       = 'winbank/winpay_form';
  protected $_winCurrencyCodes    = array('EUR' => 978, 'GBP' => 826, 'USD' => 840);

  //get data options
  
  protected $_canReviewPayment  = true;
  protected $_canAuthorize                = true;
  
  protected $_canFetchTransactionInfo = true;


  public function acceptPayment(Mage_Payment_Model_Info $payment) 
  {
        parent::acceptPayment($payment);
        //perform gateway actions to remove Fraud flags. Capture should not occur here
        return true;
        //returning true will trigger a capture on any existing invoices, otherwise the admin can manually Invoice the order
    }


    public function denyPayment(Mage_Payment_Model_Info $payment) {
        parent::denyPayment($payment);
        //if your payment gateway supports it, you should probably void any pre-auth
        return true;  
    }



  //from config
  public function getWinTicketingurl() {
    return $this->getConfigData("win_ticketingurl");
  }

  public function getWinPaymenturl() {
    return $this->getConfigData("win_paymenturl");
  }

  public function getWinAcquirerid() {
    return $this->getConfigData("win_acquirerid");
  }

  public function getWinMerchantid() {

    $cuurentCurrency = Mage::app()->getStore()->getCurrentCurrencyCode();

    if($cuurentCurrency=="USD")
      return $this->getConfigData("win_merchantid_usd");
    else if($cuurentCurrency=="GBP")
      return $this->getConfigData("win_merchantid_gbp");
    else
      return $this->getConfigData("win_merchantid");
  }

  public function getWinPosid() {
    
    $cuurentCurrency = Mage::app()->getStore()->getCurrentCurrencyCode();

    if($cuurentCurrency=="USD")
      return $this->getConfigData("win_posid_usd");
    else if($cuurentCurrency=="GBP")
      return $this->getConfigData("win_posid_gbp");
    else
      return $this->getConfigData("win_posid");
  }

  public function getWinUsername() {
    return $this->getConfigData("win_username");
  }

  public function getWinPassword() {
    return md5($this->getConfigData("win_password"));
  }

  public function getSupportEmails() {
    return $this->getConfigData("win_supportmails");
  }


  public function getOrderStatus() {
    return $this->getConfigData('order_status');
  }

  public function getDebug() {
    return $this->getConfigData('debug');
  }

  public function getAllowedCurrency() {
    return explode(",",$this->getConfigData('Currency'));
  }

  //other
  public function getSession() {
    return Mage::getSingleton('winbank/winpay_session');
  }

  public function getCheckout() {
    return Mage::getSingleton('checkout/session');
  }

  public function getQuote() {
    return $this->getCheckout()->getQuote();
  }

  public function getCustomer() {
    if (empty($this->_customer)) {
      $this->_customer = Mage::getSingleton('customer/session')->getCustomer();
    }
    return $this->_customer;
  } 

  public function getEmail() {
    return (string)Mage::getSingleton('customer/session')->getCustomer()->getEmail();
  }

  public function getLogPath() {
    return Mage::getBaseDir() . '/var/log/winbnk.log';
  }

  //get current currency code from winbank codes
  public function getCurrencyCode()
  {
    $currentCurrency = Mage::app()->getStore()->getCurrentCurrencyCode();
    return $this->_winCurrencyCodes[$currentCurrency];
  }


  //init the merchant reference for ticketink service
  public function initMerchantReference($orderId)
  {
    Mage::getModel('checkout/session')->setMerchantReference($orderId);

    return $orderId;

    // $tm = time();
    // $checkoutSession = $this->getCheckout();
    // $checkoutSession = $checkoutSession->getData();
    // $cip = $checkoutSession["_session_validator_data"]["remote_addr"];
    
    // //set in session to use later
    // Mage::getModel('checkout/session')->setMerchantReference(base64_encode($cip.$tm));

    // return base64_encode($cip.$tm);
  }

  //get order final ammount
  public function getAmmount()
  {
    $quote = Mage::getModel('checkout/session')->getQuote();
    $quoteData= $quote->getData();
    $grandTotal=$quoteData['grand_total'];
    return $grandTotal;   
  }	
  
  
	//additional fields in redirect form
	public function addWinpayFields($form) 
  {
    
    //init winbank payment model
    $paymentModel = Mage::getModel("winbank/winpay");
   

    //get data that you need for payment
    $acquirerId             = $paymentModel->getWinAcquirerid();
    $merchantId             = $paymentModel->getWinMerchantid();
    $posId                  = $paymentModel->getWinPosid();
    $username               = $paymentModel->getWinUsername();
    $language_code          = str_replace("_","-",Mage::app()->getLocale()->getLocaleCode());

    $merchantReference      = Mage::getSingleton('checkout/session')->getMerchantReference();
    $paramBackLink          = "";


    Mage::log("merchant reference sent in post : $merchantReference",null,"winbnk.log");

    $form->addField("AcquirerId", 'hidden', array('name' => 'AcquirerId', 'value' => $acquirerId));
    $form->addField("MerchantId", 'hidden', array('name' => 'MerchantId', 'value' => $merchantId)); 
    $form->addField("PosId", 'hidden', array('name' => 'PosId', 'value' => $posId)); 
    $form->addField("User", 'hidden', array('name' => 'User', 'value' => $username)); 
    $form->addField("LanguageCode", 'hidden', array('name' => 'LanguageCode', 'value' => $language_code)); 
    $form->addField("MerchantReference", 'hidden', array('name' => 'MerchantReference', 'value' => $merchantReference)); 
    $form->addField("ParamBackLink", 'hidden', array('name' => 'ParamBackLink', 'value' => $paramBackLink)); 
    $form->addField("", 'submit', array('name' => '', 'value' => "Check out"));  

  
		return $form;

	} 
 
  //checkout form
  public function createFormBlock($name) {
        return;
    $block = $this->getLayout()->createBlock('winbank/winpay_form', $name)
            ->setMethod('winpay')
            ->setPayment($this->getPayment())
            ->setTemplate('converge/winbank/winpay/form.phtml');

    return $block;
  }

  //check if currency exists in availiable values
  public function validate() {
    parent::validate();
    return;
    $currency_code = $this->getQuote()->getBaseCurrencyCode();
    
    Mage::log("@@ validate currency code: $currency_code ".print_r($this->getAllowedCurrency(),1));

    if (!in_array($currency_code,$this->getAllowedCurrency())) 
    {
      Mage::throwException(Mage::helper('winbank')->__('Selected currency code ('.$currency_code.') is not compatible'));
    }
    return $this;
  }

  

  //redirect url
  public function getOrderPlaceRedirectUrl() 
  {
    return Mage::getUrl('winbank/winpay/redirect');
  }
    

  public function initialize($paymentAction, $stateObject)
  {
      Mage::log('@@ Called ' . __METHOD__ . ' with payment ' . $paymentAction, null, 'winbnk.log');

      parent::initialize($paymentAction, $stateObject);
      
      //Payment is also used for refund and other backend functions.
      //Verify this is a sale before continuing.
      if($paymentAction != 'authorize_capture')
      {
          return $this;
      }
      
      //Set the default state of the new order.
      $state = Mage_Sales_Model_Order::STATE_PENDING_PAYMENT; // state now = 'pending_payment'
      $stateObject->setState($state);
      $stateObject->setStatus('pending_payment');
      $stateObject->setIsNotified(false);
      
      
      return $this;
  }

}

?>