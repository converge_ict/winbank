<?php

class Converge_Winbank_Model_Source_Type {
	
	public function toOptionArray() {
		return array(
			array('value' => 'authorisation', 'label' => Mage::helper('winbank')->__('Authorization')),
			array('value' => 'sale', 'label' => Mage::helper('winbank')->__('Sale'))
		);
		
	}
}

?>