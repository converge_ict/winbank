<?php

class Converge_Winbank_Model_Source_Currencies {

	public function toOptionArray() {

		return array(
			array('value' => 'EUR', 'label' => Mage::helper('winbank')->__('Euro')),
			array('value' => 'GBP', 'label' => Mage::helper('winbank')->__('British Pound')),
			array('value' => 'USD', 'label' => Mage::helper('winbank')->__('US Dollar'))
		);

	}
}

?>