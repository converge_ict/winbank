<?php
Class Converge_Winbank_Model_Observer extends Varien_Object
{


	public function initWinTicket($observer)
    {
        $order = $observer->getOrder(); 

        //Mage::log("~~~".print_r($order->getData(),1),null,"winbnk.log");

        $paymentModel = Mage::getModel("winbank/winpay");
        

        //get data that you need for ticketing
        $ticketingServiceUrl    = $paymentModel->getWinTicketingurl();
        $acquirerId             = $paymentModel->getWinAcquirerid();
        $merchantId             = $paymentModel->getWinMerchantid();
        $posId                  = $paymentModel->getWinPosid();
        $username               = $paymentModel->getWinUsername();
        $password               = $paymentModel->getWinPassword();
        $requestType            = "02"; // 02: immediate payment 00: for initial money holding
        $currencyCode           = $paymentModel->getCurrencyCode();
        $merchantReference      = $paymentModel->initMerchantReference($order->getIncrementId());
        $amount                 = number_format($paymentModel->getAmmount(), 2, '.', '');
        $installments           = 0; // 0: no installments
        $expirePreauth          = 0; 
        $bnpl                   = 0;

        if((int) Mage::app()->getRequest()->getPost('win_installments') > 0)
            $installments = (int) Mage::app()->getRequest()->getPost('win_installments');
        
        Mage::log("MerchantReference sent in ticket $merchantReference",null,"winbnk.log");

        $fields = array(
                                'Username' => $username,
                                'Password' => $password,
                                'MerchantId' => $merchantId,
                                'PosId' => $posId,
                                'AcquirerId' => $acquirerId,
                                'MerchantReference' => $merchantReference,
                                'RequestType' => $requestType,
                                'ExpirePreauth' => $expirePreauth,
                                'Amount' => $amount,
                                'CurrencyCode' => $currencyCode,
                                'Installments' => $installments,
                                'Bnpl' => $bnpl,
                                'Parameters' => ""
                        );

        $sendData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
        <soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
          <soap:Body>
            <IssueNewTicket xmlns=\"http://piraeusbank.gr/paycenter/redirection\">
              <Request>";
        //url-ify the data for the POST
        foreach($fields as $key=>$value) 
            $sendData .="<$key>$value</$key>";
        
        $sendData .= "</Request></IssueNewTicket></soap:Body></soap:Envelope>";

        Mage::log("@@ ticketing data :".$sendData,null,"winbnk.log");     

        //open connection
        $ch = curl_init();

        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $ticketingServiceUrl);
        //curl_setopt($ch,CURLOPT_URL, "http://origin01.nibras.com:6080/auth12.php");
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-type: text/xml; charset=utf-8',"Content-Length: ".strlen($sendData)));
        curl_setopt($ch,CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $sendData);

        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        
        //execute post
        $xmlResponse = curl_exec($ch);

        Mage::log("@@  ticket response $xmlResponse",null,"winbnk.log");

        //close connection
        curl_close($ch);

        //Mage::log("@@ XML Response ".print_r($xmlResponse,1),null,"winbnk.log");
        //get the transaction ticket from the response
        $xml = simplexml_load_string(preg_replace('/soap\:/','soap',$xmlResponse));


        $tranTicketArr  = (array) $xml->soapBody->IssueNewTicketResponse->IssueNewTicketResult->TranTicket;
        if(count($tranTicketArr)==1)  
        {  
            $tranTicket     = $tranTicketArr[0];
            Mage::log("@@  ticket $tranTicket",null,"winbnk.log");
        }
        else
        {
            Mage::log("@@ ERROR no transaction ticket",null,"winbnk.log");
            return;
        }

        //save the transaction ticket in session
        Mage::getSingleton('checkout/session')->setWinTicket($tranTicket);

        return;
    }

    
}
