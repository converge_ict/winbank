<?php

$this->startSetup();

if( $this->getConnection()->isTableExists( $this->getTable('paygate/authorizenet_debug') ) != true ):

    $table = $this->getConnection()
        ->newTable($this->getTable('paygate/authorizenet_debug'))
        ->addColumn(
            'debug_id',
            Varien_Db_Ddl_Table::TYPE_INTEGER,
            null,
            array(
                'identity'  => true,
                'nullable'  => false,
                'primary'   => true,
            ),
            'Debug ID'
        )
        ->addColumn(
            'request_body',
            Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
            array(
                'nullable'  => true,
            ),
            'Request Body'
        )
        ->addColumn(
            'response_body',
            Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
            array(
                'nullable'  => true,
            ),
            'Response Body'
        )
        ->addColumn(
            'request_serialized',
            Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
            array(
                'nullable'  => true,
            ),
            'Request serialized'
        )
        ->addColumn(
            'result_serialized',
            Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
            array(
                'nullable'  => true,
            ),
            'Result Serialized'
        )
        ->addColumn(
            'request_dump',
            Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
            array(
                'nullable'  => true,
            ),
            'Request Dump'
        )
        ->addColumn(
            'result_dump',
            Varien_Db_Ddl_Table::TYPE_TEXT, '64k',
            array(
                'nullable'  => true,
            ),
            'Result Dump'
        )
        ->setComment('authorizenet_debug Table');
    $this->getConnection()->createTable($table);
    
endif;

$this->endSetup();