<?php

class Converge_Winbank_Block_Winpay_Form extends Mage_Payment_Block_Form {

    protected function _construct() {
    
        $this->setTemplate('converge/winbank/winpay/form.phtml');
        parent::_construct();
        
    }
    
}
