<?php

class Converge_Winbank_Block_Winpay_Redirect extends Mage_Core_Block_Abstract {

	protected function _toHtml() {
	   
        $winpay = Mage::getModel('winbank/winpay');

        $form = new Varien_Data_Form();
        $form->setAction($winpay->getWinPaymenturl())
            ->setId('winpay_checkout')
            ->setName('winpay_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
		
        $form = $winpay->addWinpayFields($form);
		
        $html = '<html><body>';
        $html.= $this->__('You will be redirected to Winbank in a few seconds.');
        $html.= $form->toHtml();
        $html.= '<script type="text/javascript">document.getElementById("winpay_checkout").submit();</script>';
        $html.= '</body></html>';

        return $html;
        
    }
    
}
